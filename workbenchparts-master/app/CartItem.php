<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    public $guarded = [];
    
    public function item()
    {
        return $this->hasOne('App\Item', 'iikwrd', 'sku');
    }

    public function price()
    {
        return $this->hasOne('App\Price', 'sku', 'sku');
    }

    /**
     * cart total retail price in money format
     *
     * @param String $store
     * @return String
     */
    public static function totalRetailPrice($store)
    {
        $items = self::select('sku','quantity')->forStore($store)->with('price')->get();
        
        $totalPrice = 0;

        foreach($items as $item) {
            $price = $item->price ? $item->price->price : 0;
            $totalPrice += ($item->quantity * $price);
        }

        return money_format('%.2n', $totalPrice);
    }

    public function scopeForStore($query, $store)
    {
        return $query->where('store', $store);
    }
}
