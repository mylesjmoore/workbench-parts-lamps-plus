<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemExtension extends Model
{
    protected $connection = 'ibmi';
    protected $table = 'IWITEMTL';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;
    public $primaryKey = 'mwskuno';

    public static function maxOrderableQuantity($sku)
    {
        $item = self::where('mwskuno', $sku)->first();
        
        return $item ? $item->mwexpmaxoq : 0;
    }
}
