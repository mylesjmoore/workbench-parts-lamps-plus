<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $connection = 'ibmi';
    protected $table = 'imcmpyfl';
    public $incrementing = false;
    public $timestamps = false;

    public static function openLampsBrickLocations()
    {
        return self::select('lploc', 'mpadr1', 'mpcity', 'mpstte', 'mpzpcd')
            ->join('lpctrlfl', function($join) {
                $join->on('lploc', '=', 'mploc#')
                    ->on('lpcmpy', '=', 'mpcmp#');
            })
            ->where('lpcmpy', '02')
            ->where('lpstyp', 'B')
            ->where('lpclosed', '0')
            ->get();
    }

    public function getCityAttribute()
    {
        return ucwords(strtolower($this->mpcity));
    }
}