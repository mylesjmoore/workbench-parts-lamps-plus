<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $connection = 'ibmi';
    protected $table = 'WRKORDDTTL';

    protected $guarded = [];

    public function item()
    {
        return $this->hasOne('App\Item', 'iikwrd', 'sku');
    }
}
