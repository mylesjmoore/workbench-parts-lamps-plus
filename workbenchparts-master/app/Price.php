<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $connection = 'ibmi';
    protected $table = 'webpricevw';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'sku';
    public $timestamps = false;    
}
