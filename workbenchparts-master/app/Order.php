<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $connection = 'ibmi';
    protected $table = 'WRKORDHDTL';

    protected $guarded = [];

    public function items()
    {
        return $this->hasMany('App\OrderItem');
    }

    //return formated total retail price
    public function totalRetailPrice()
    {
        $totalPrice = 0;

        foreach ($this->items as $item) {
            $totalPrice += $item->quantity * $item->item->price->price;
        }

        return money_format('%.2n', $totalPrice);
    }
    
    //get displayable order status
    public function status()
    {
        $order = DB::connection('ibmi')->table('ibohdgfl')
                                ->where('bhord#', $this->order_number)
                                ->where('bhcmp#', '03')
                                ->where('bhloc#', '99')
                                ->first();
        
        $statusCode = $order ? $order->bhstts : 'RQ';

        $statuses = [
            'RQ' => 'Requested',
            'CL' => 'Cancelled',
            'HL' => 'Held',
            'PD' => 'Store Sales',
            'PP' => 'Pending Processing',
            'IP' => 'In Process',
            'IN' => 'Invoiced',
            'UP' => 'Warehouse Shipment Update',
        ];

        return array_key_exists($statusCode, $statuses) ? $statuses[$statusCode] : 'Unknown';
    }
}
