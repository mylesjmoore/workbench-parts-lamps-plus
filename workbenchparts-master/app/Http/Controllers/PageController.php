<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function workbench()
    {
        return view('items')->with('items', Item::displayable()->subClass('1')->paginate(18));
    }

    public function expense()
    {
        return view('items')->with('items', Item::displayable()->subClass('2')->paginate(18));
    }

    public function display()
    {
        return view('items')->with('items', Item::displayable()->subClass('3')->paginate(18));
    }

}
