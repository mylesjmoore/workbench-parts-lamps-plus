<?php

namespace App\Http\Controllers;

use App\CartItem;
use App\ItemExtension;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class CartItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($store)
    {
        $cartItems = CartItem::forStore($store)->with('item:iikwrd,iidesc')->get();

        $totalRetailPrice = CartItem::totalRetailPrice($store);

        return view('/cart', compact('cartItems','totalRetailPrice'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $maxOrderableQuantity = ItemExtension::maxOrderableQuantity($request->sku);

        $request->validate([
            'store' => 'required',
            'sku' => 'required',
            'quantity' => "required|numeric|min:1|max:{$maxOrderableQuantity}"
        ], [
            'max' => "You can only order {$maxOrderableQuantity} of these." 
        ]);
        
        //validate that the sku is not already in the cart for this store
        $request->validate([
            'sku' => Rule::unique('cart_items')->where(function ($query) use($request) {
                return $query->where('store', $request->store);
            })
        ], ['unique' => 'This sku is already in the cart.']);

        CartItem::create([
            'store' => $request->store,
            'sku' => $request->sku,
            'quantity' => $request->quantity
        ]);

        return back()->with('message', 'Item added to cart.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CartItem  $cartItem
     * @return \Illuminate\Http\Response
     */
    public function show($store)
    {
        $cartItems = CartItem::where('store', $store)->with('item:iikwrd,iidesc')->get();

        return view('/cart', compact('cartItems'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CartItem  $cartItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CartItem $cartItem)
    {
        $maxOrderableQuantity = ItemExtension::maxOrderableQuantity($cartItem->sku);

        $request->validate([
            'quantity' => "required|numeric|min:1|max:{$maxOrderableQuantity}"
        ],[
            'max' => "You can only order {$maxOrderableQuantity} of these." 
        ]);

        $cartItem->update([
            'quantity' => $request->quantity
        ]);

        return back()->with('message', 'Quantity updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CartItem  $cartItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(CartItem $cartItem)
    {
        $cartItem->delete();

        return back()->with('message', 'Item removed from cart.');        
    }

}