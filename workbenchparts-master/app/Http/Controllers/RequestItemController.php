<?php

namespace App\Http\Controllers;

use App\RequestItem;
use Illuminate\Http\Request;
use App\Mail\RequestItemMail;

class RequestItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($store)
    {
        $items = RequestItem::where('store', $store)->orderBy('created_at', 'desc')->paginate(10);

        return view('requestItem.index', compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate request
        $request->validate([
            'store' => 'required',
            'sku' => 'required',
            'quantity' => "required|numeric|min:1"
        ]);

        //store the request item
        RequestItem::create([
            'store' => $request->store,
            'sku' => $request->sku,
            'quantity' => $request->quantity
        ]);
        
        //get request item email addresses
        $address = env('MAIL_REQUEST_ITEM_ADDRESS','parts@lampsplus.com');
        $addressCC = env('MAIL_REQUEST_ITEM_ADDRESS_CC','LMershimer@lampsplus.com');

        //send an email for the requested item
        \Mail::to($address)->cc($addressCC)->send(new RequestItemMail($request->store, $request->sku, $request->quantity));
        
        //redirect with message
        return redirect("/requestItem/store/{$request->store}")->with('message', 'The Item Was Successfully Requested!');
    }
}
