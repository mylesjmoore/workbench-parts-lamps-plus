<?php

namespace App\Http\Controllers;

use App\Order;
use App\CartItem;
use App\OrderItem;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($store)
    {
        $orders = Order::where('store', $store)->with('items')->orderBy('created_at', 'desc')->paginate(10);

        return view('order.index', compact('orders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'store' => 'required'
        ]);

        $store = $request->store;

        $items = CartItem::forStore($store)->get();

        $order = Order::create([
            'store' => $store
        ]);

        foreach($items as $item) {
            OrderItem::create([
                'order_id' => $order->id,
                'sequence' => OrderItem::where('order_id', $order->id)->max('sequence') + 1,
                'sku' => $item->sku,
                'quantity' => $item->quantity
            ]);

            CartItem::where('id', $item->id)->delete();
        }

        return redirect("/order/{$order->id}")->with('message', 'The Order Was Successfully Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view('order.show', compact('order'));
    }
}
