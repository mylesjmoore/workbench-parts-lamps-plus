<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Item extends Model
{
    protected $connection = 'ibmi';
    protected $table = 'IMITEMFL';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;
    public $primaryKey = 'IIKWRD';

    /**
     * price relationship
     *
     * @return HasOne
     */
    public function price()
    {
        return $this->hasOne('App\Price', 'sku', 'iikwrd');
    }

    /**
     * item extension relationship
     *
     * @return HasOne
     */
    public function extension()
    {
        return $this->hasOne('App\ItemExtension', 'mwskuno', 'iikwrd');
    }

    /**
     * format price for dollar amount
     *
     * @return String
     */
    public function formattedPrice()
    {
        return money_format('%.2n', $this->price->price);
    }

    /**
     * return image path
     *
     * @return String
     */
    public function imagePath()
    {
        return "https://image.lampsplus.com/is/image/b9gt8/{$this->iikwrd}.jpg?qlt=100&amp;wid=300&amp;hei=300&amp;fmt=jpeg";
    }

    /**
     * where department
     *
     * @param Query $query
     * @param String $department
     * @return Query
     */
    public function scopeDepartment($query, $department)
    {
        return $query->where('iidept', $department);
    }

    /**
     * where class
     *
     * @param Query $query
     * @param String $class
     * @return Query
     */
    public function scopeClass($query, $class)
    {
        return $query->where('iiclas', $class);
    }

    /**
     * where subclass
     *
     * @param Query $query
     * @param String $subclass
     * @return Query
     */
    public function scopeSubClass($query, $subclass)
    {
        return $query->where('iisbcl', $subclass);
    }

    /**
     * where item can be displayed
     * only get items in sku status CURR, CATAL, or DISC% with quantity
     *
     * @param Query $query
     * @return Query
     */
    public function scopeDisplayable($query)
    {
        return $query->whereRaw("mxskust in ('CURR','CATAL')")
            ->orWhereRaw("mxskust like ('DISC%') and dec(ilqtoh - (ilex02 + ilex03 + ilex04),9,0) > 0");
    }

    /**
     * filter items by request for description if it exists
     *
     * @param Query $query
     * @return Query
     */
    public function scopeSearchDescription($query)
    {
        if(! request()->has('description')) return $query;

        $description = \strtoupper(request()->description);

        return $query->whereRaw("upper(iidesc) like '%{$description}%'");
    }

    /**
     * fetch displayable items
     *
     * @return Collection
     */
    public static function displayable()
    {
        return self::select(
                'iikwrd', 'iidesc', 'mxskust', 'mwexpmaxoq',
                'dec(ilqtoh - (ilex02 + ilex03 + ilex04),9,0) as quantity'
            )
            ->join('imlitmfl', function($join) {
                $join->on('iikwrd', '=', 'ilsku#')
                    ->where('ilcmp#', '=', '03')
                    ->where('illoc#', '=', '99');
            })
            ->join('ixitemfl', 'iikwrd', '=', 'mxsku#')
            ->join('iwitemtl', 'iikwrd', '=', 'mwskuno')
            ->department('1')
            ->class('18')
            ->displayable()
            ->searchDescription();
    }

}
