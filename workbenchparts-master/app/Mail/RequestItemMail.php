<?php

namespace App\Mail;

use App\Item;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestItemMail extends Mailable
{
    use Queueable, SerializesModels;

    public $store;
    public $sku;
    public $quantity;
    public $vendorNumber;
    public $vendorStyleNumber;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($store, $sku, $quantity)
    {
        $this->store = $store;
        $this->sku = $sku;
        $this->quantity = $quantity;

        $item = ITEM::select('iivd#1 as vendor', 'mxvnsty as style')
            ->join('ixitemfl', 'iikwrd', '=', 'mxsku#')
            ->where('iikwrd', $sku)->first();

        $this->vendorNumber = $item->vendor;
        $this->vendorStyleNumber = $item->style;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Store {$this->store} Requesting Catalog Part")
            ->markdown('emails.requestItemMail');
    }
}
