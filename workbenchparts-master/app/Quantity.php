<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quantity extends Model
{
    protected $connection = 'ibmi';
    protected $table = 'redwhqtyvw';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'sku';
    public $timestamps = false; 
}
