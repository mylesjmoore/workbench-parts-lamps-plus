# WorkBenchParts
###### Jira: as-6347
###### Author: Andrew Beatrice
###### Date: 09/11/19
###### Description: Create web open application for stores to allow them to order Workbench Parts, Expense Items, and Display Items from the warehouse
---

#### Local Development:

copy the source code to your local dev enviornment
```sh
$ git clone https://lpgitlab01.lpdomain.com/programmer/workbenchparts.git
$ cd workbenchparts
```

copy the env file and nginx conf files and restart nginx and php-fpm
```sh
$ cp .env.example .env
$ cp workbenchparts.conf /etc/nginx/conf.d/workbenchparts.conf
$ sudo systemctl restart nginx
$ sudo systemctl restart php72-php-fpm.service
```

update the local .env file in your project root to have the correct redis & phpuser password and update the email addresses to your own

create the database in the local mysql database
```sh
$ mysql -u root -p
$ create database workbenchparts
$ exit
```

install php dependancies
```sh
$ composer install
```

perform database migrations to create the tables locally
```sh
$ php artisan migrate
```

add DNS entry to host machine (windows) host file to point to the new nginx server block, file located: C:\Windows\System32\drivers\etc
get the ip address from the vagrant file in your vagrant machine folder: c:dev
```sh
#example 192.168.28.10 workbenchparts.dev
<ip_address_to_vagrant_machine> workbenchparts.dev
```

#### Initial Deployment:

##### Make workbenchparts database:
```sh
$ ssh vagrant@lpphpprod03
$ mysql -u root -p
$ create database workbenchparts
$ exit
```

##### Install Code
```sh
$ ssh vagrant@lpphpprod03
$ cd /home/vagrant/html
$ sudo git clone https://lpgitlab01.lpdomain.com/programmer/workbenchparts.git
$ cd workbenchparts
$ composer install
$ php artisan migrate
$ sudo chmod -R 777 /home/vagrant/html/workbenchparts/
```

##### Update Code
```sh
$ ssh vagrant@lpphpprod03
$ cd /home/vagrant/html/workbenchparts
$ php artisan down
$ sudo git pull
$ composer install
$ php artisan migrate
$ php artisan up
```

#### Application Specifications:
Users will visit workbenchparts.lpdomain.com to see a catalog of items. The user will be forced to select what store location they are at. The store location will be stored in local storage so that the user will not need to select it again unless they change computers or log in under another user. The catalog has tabs to select between workbench parts, expense itens, and display items. The user will find the item they want to order, click an add to cart button, input a quantity, and confirm the selection. The user will visit the cart to display the items placed in the cart. In the cart the user can: update item order quantity, remove the item from the cart, or continue to place the order. When the user select's continue to order, the cart items will be displayed again along with the store information and will be prompted to confirm the order. Users can visit the order's page to display previous orders. This will display the order status and the user can select list to display a list of items that where ordered.

##### WorkBench order header & detail:
When a user creates an order the order is recorded in the file: moms.wrkordhdtl
The ordered items are recorded in the file: moms.wrkorddttl

##### IBO Records:
Ibo header and detail records are created on the IBMi with the rpg program: OWBP01R. This program runs once an hour and converts work bench orders that have not been processed into IBO records. Unprocessed orders are identified by field moms.wrkordhdtl.processed = 0

##### WM interface
When IBO records are created, the field bhmoms = 'TBA' and BHPR = 'R". This will trigger another process WMS_RSHORD that is always running to generate pick tickets from the IBO records.

##### Item's Department | Class | Subclass:
 - Workbench Parts: 1 | 18 | 1
 - Expense Items: 1 | 18 | 2
 - Display Items: 1 | 18 | 3

##### Order Validations
 - User can not order more than the max orderable quantity for an item: IWITEMTL.MWEXPMAXOQ
 