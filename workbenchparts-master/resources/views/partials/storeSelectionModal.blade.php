<div class="modal fade" id="storeSelectionModal" role="dialog" aria-labelledby="storeSelectionModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addToCartModalTitle">Select Store</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="store-select-action">
                <div class="modal-body">
                    <div class="d-flex justify-content-center">
                        <div class="d-flex flex-column">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label font-weight-bold" for="store">Store:</label>
                                <div class="col-sm-9" style="flex: 1 1 auto;">
                                    <select class="form-control" id="store-select-input" name="store">
                                        @foreach($stores as $store)
                                            <option value="{{$store->lploc}}">{{$store->lploc}} {{$store->city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>    
    </div>
</div>