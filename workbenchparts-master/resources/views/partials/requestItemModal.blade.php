<div class="modal fade" id="requestItemModal" role="dialog" aria-labelledby="requestItemModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border border-warning">
            <div class="modal-header">
                <h5 class="modal-title" id="addToCartModalTitle">Request Item</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="add-form-action" action="/requestItem" method="POST">
                @csrf
                <input type="hidden" class="skuModalInput" name="sku" value="">
                <input type="hidden" class="storeModalInput" name="store" value="">
                <div class="modal-body">
                    <div class="d-flex justify-content-center">
                        <div class="d-flex flex-column">
                            <h1 class="text-center mb-0" class="modalTitle"></h1>
                            <p class="text-center text-muted mt-0" class="modalDescription"></p>
                            <img src="" class="modalImg">
                            <p class="px-3 mt-3">
                                <i class="material-icons text-warning">warning</i>
                                <span class="ml-1">
                                    This item has a sku status which prevents it from being ordered here.
                                </span>
                            </p>
                            <p class="px-3">
                                Would you like to submit a request for this item?
                            </p> 
                            <div class="form-group row mt-4">
                                <label class="col-sm-8 col-form-label font-weight-bold" for="modalQuantity">Requested Quantity:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control form-control-lg text-center modalQuantity" id="modalQuantity" name="quantity" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </form>
        </div>    
    </div>
</div>
