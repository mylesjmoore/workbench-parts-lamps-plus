<div class="modal fade" id="confirmOrderModal" tabindex="-1" role="dialog" aria-labelledby="confirmOrderModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmOrderModalTitle">Confirm Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-borderless" style="max-width: 970px;">
                    <thead class="border-bottom">
                        <tr>
                            <th scope="col" class="text-left">Item</th>
                            <th scope="col" class="text-center">Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cartItems as $cartItem)
                            <tr class="border-bottom">
                                <td class="align-middle">
                                    <div class="d-flex align-items-center">
                                        <img class="mr-2" src="{{$cartItem->item->imagePath()}}" alt="{{$cartItem->sku}}" height="80" width="80">
                                        <div class="d-inline-flex flex-column">
                                            <h4 class="m-0">{{$cartItem->sku}}</h4>
                                            <p class="m-0"><small>{{$cartItem->item->iidesc}}</small></p>
                                        </div>
                                    </div>
                                </td>
                                <td class="align-middle text-center">
                                    {{$cartItem->quantity}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <table class="table table-sm table-borderless max-width-md text-right">
                    <tr>
                        <th>Total Items:</th>
                        <td>{{$cartItems->sum('quantity')}}</td>
                    </tr>
                    <tr>
                        <th>Total Retail Price:</th>
                        <td>{{$totalRetailPrice}}</td>
                    </tr>
                    <tr>
                        <th>Store Number:</th>
                        <td><span class="store-number">Unselected</span></td>
                    </tr>
                    <tr>
                        <th>Store Address:</th>
                        <td>
                            <div class="d-flex flex-column">
                                <span class="store-address"></span>
                                <span class="store-city-state-zip"></span>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <form id="confirm-order-action" action="/order" method="POST">
                    @csrf
                    <input type="hidden" id="order-store" name="store" value="">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-success">Confirm</button>
                </form>
            </div>
        </div>
    </div>
</div>