<div class="modal fade" id="deleteFromCartModal" role="dialog" aria-labelledby="deleteFromCartModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteFromCartModalTitle">Remove Item From Cart</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="delete-cart-action" action="" method="POST">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    <div class="d-flex justify-content-center">
                        <div class="d-flex flex-column">
                            <h1 class="text-center mb-0" id="modalTitle"></h1>
                            <p class="text-center text-muted mt-0" id="modalDescription"></p>
                            <img src="" id="modalImg">
                            <div class="form-group row mt-4">
                                <label class="col-sm-8 col-form-label font-weight-bold" for="quantity">Quantity:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control form-control-lg text-center" id="quantity" name="quantity" value="" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger">Confirm</button>
                </div>
            </form>
        </div>    
    </div>
</div>