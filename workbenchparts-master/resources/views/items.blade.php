@extends('layouts.app')

@section('content')

    @include('nav.tablinks')

    @include('partials.flashMessages')

    @if(count($items) > 0)
        <div class="card-deck">
            @foreach($items as $key => $item)

                <!-- need to create a line break per x number of items on page depending on how wide the page is -->
                @if($key % 2 == 0)<div class="w-100 d-none d-sm-block d-md-none"></div>@endif <!-- wrap every 2 items on sm-->
                @if($key % 3 == 0)<div class="w-100 d-none d-md-block d-lg-none"></div>@endif <!-- wrap every 3 items on md-->
                @if($key % 4 == 0)<div class="w-100 d-none d-lg-block d-xl-none"></div>@endif <!-- wrap every 4 items on lg-->
                @if($key % 6 == 0)<div class="w-100 d-none d-xl-block"></div>@endif           <!-- wrap every 6 items on xl-->

                <a href="#" data-toggle="modal" 
                    data-sku="{{$item->iikwrd}}" data-description="{{$item->iidesc}}"
                    data-target="{{ $item->mxskust == 'CATAL' ? '#requestItemModal' : '#addToCartModal' }}"
                >
                    <div class="card mb-4 hoverable" style="max-width: 17rem;">
                        <img class="card-img-top p-3" src="{{$item->imagePath()}}" alt="{{$item->iikwrd}}">
                        <div class="d-flex flex-column card-body">
                            <div class="d-flex justify-content-between">
                                <h5 class="card-title">{{$item->iikwrd}}</h5>
                                @if($item->mxskust == 'CATAL')
                                    <a href="#" title="Request Item" 
                                        data-toggle="modal" data-target="#requestItemModal"
                                        data-sku="{{$item->iikwrd}}" data-description="{{$item->iidesc}}">
                                            <i class="material-icons">mail_outline</i>
                                    </a>
                                @else
                                    <a href="#" title="Add to Cart" 
                                        data-toggle="modal" data-target="#addToCartModal"
                                        data-sku="{{$item->iikwrd}}" data-description="{{$item->iidesc}}">
                                            <i class="material-icons">add_shopping_cart</i>
                                    </a>
                                @endif
                            </div>
                            <small class="mb-2">{{$item->iidesc}}</small>
                            <div class="d-flex justify-content-between mt-auto">
                                <small>Warehouse Quantity:</small>
                                <small>{{$item->quantity}}</small>
                            </div>
                            <div class="d-flex justify-content-between mt-auto">
                                <small>Max Orderable Quantity:</small>
                                <small>{{$item->mwexpmaxoq}}</small>
                            </div>
                        </div>
                    </div>
                </a>

            @endforeach
        </div>
    @else
        <div class="d-flex justify-content-center">
            <div class="jumbotron">
                <div class="container mx-5">
                    <h1 class="display-4">No Items Found!</h1>
                    <p class="lead">Try another item description in the search criteria...</p>
                </div>
            </div>
        </div>
    @endif

    {{$items->links()}}

    @include('partials.addToCartModal')
    @include('partials.requestItemModal')

@endsection

@section('js')
<script>
    $(function () {
        $('#addToCartModal, #requestItemModal').on('show.bs.modal', function (event) {
            console.log('hit');
            
            let data = $(event.relatedTarget).data();

            $('.skuModalInput').val(data.sku);
            $('.storeModalInput').val(localStorage.getItem("store"));
            $(".modalTitle").html(data.sku);
            $(".modalDescription").html("<small>"+data.description+"</small>");
            $(".modalImg").attr("src","http://image.lampsplus.com/is/image/"+data.sku+".fpx?qlt=100&amp;wid=300&amp;hei=300&amp;fmt=jpeg");
            $('.modalQuantity').val("1");
            
        });

        $('#addToCartModal, #requestItemModal').on('shown.bs.modal', function (event) {
            $('.modalQuantity').focus();
        });
    });
</script>
@endsection
