@component('mail::message')

# Store {{$store}} Requesting Catalog Part

A store would like an item to be sent to them. They are unable to order the item through the workbench parts program due to a lack of stock in the warehouse. 
Please have an order shipped to the Redland’s warehouse so we are able to fill this request for the store below.

Here are the details:
```
 - Store: {{$store}}
 - Sku: {{$sku}}
 - Quantity: {{$quantity}}
 - Vendor #: {{$vendorNumber}}
 - Vendor Style #: {{$vendorStyleNumber}}
```

@endcomponent
