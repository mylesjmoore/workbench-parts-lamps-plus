<nav class="navbar navbar-light bg-light fixed-top shadow-sm">
    <div class="ml-4">
        <a class="navbar-brand" href="/">
            <img class="mr-3" src="/svg/lampsPlusLogo.svg" alt="" height="20">
        </a>
    </div>
    <div class="mr-4 d-flex align-items-center">
        <div class="d-flex flex-column text-right mr-2">
            <p class="my-0 py-0"><span class="store-address"></span></p>
            <p class="my-0 py-0"><span class="store-city-state-zip"></span></p>
        </div>
        <div class="d-flex flex-column text-center mr-3">
            <div class="col-sm-12" style="flex: 1 1 auto;">
                <h5 class="py-0 my-0">
                    <p class="m-0 p-0">Store: <span class="store-number">Unselected</span></p>
                </h5>
            </div>
            <div>
                <p class="m-0 p-0"><button type="submit" class="btn btn-sm btn-link my-0 py-0" data-toggle="modal" data-target="#storeSelectionModal">
                    Change Store
                </button></p>
            </div>
        </div>
        <div>
            <a class="btn btn-primary" href="" id="request-items-link" title="Requested Items">
                <i class="material-icons d-block d-lg-none">view_list</i> 
                <span class="d-none d-lg-block">
                    <i class="material-icons">view_list</i> 
                    Requested Items
                </span>
            </a>
            <a class="btn btn-primary" href="" id="orders-link" title="Orders">
                <i class="material-icons d-block d-lg-none">local_shipping</i> 
                <span class="d-none d-lg-block">
                    <i class="material-icons">local_shipping</i> 
                    Orders
                </span>
            </a>
            <a class="btn btn-primary" href="" id="cart-link" title="Orders">
                <i class="material-icons d-block d-lg-none">shopping_cart</i> 
                <span class="d-none d-lg-block">
                    <i class="material-icons">shopping_cart</i> 
                    Cart
                </span>
            </a>
        </div>
    </div>
</nav>