<div class="row mt-5 mb-5">
    <div class="col-sm-12 mt-3"> 	
        <div class="d-flex justify-content-between">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('workbench') ? 'active' : '' }}" href="/workbench">
                        Workbench Parts
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('expense') ? 'active' : '' }}" href="/expense">
                        Expense Items
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('display') ? 'active' : '' }}" href="/display">
                        Display Items
                    </a>
                </li>
            </ul>
            <form class="form" action="{{url()->current()}}" method="GET">
                <div class="input-group">
                    <input type="text" class="form-control" name="description" placeholder="Item Description" value="{{ request('description') }}">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>