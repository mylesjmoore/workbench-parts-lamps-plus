@extends('layouts.app')

@section('content')

    @include('partials.flashMessages')

    <div class="mt-5 mb-3">
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <h1>Cart</h1>
            </div>
            <div>
                <a href="/" class="btn btn-secondary pr-3"><i class="material-icons">arrow_left</i> Return To Catalog</a>
            </div>
        </div>
    </div>

    @if(count($cartItems) > 0)        
        <div class="d-flex justify-content-center">
            <table class="table table-borderless max-width-md">
                <thead class="border-bottom">
                    <tr>
                        <th scope="col" class="text-left">Item</th>
                        <th scope="col" class="text-center">Quantity</th>
                        <th scope="col" class="text-center">Remove</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cartItems as $cartItem)
                        <tr class="border-bottom">
                            <td class="align-middle">
                                <div class="d-flex align-items-center">
                                    <img class="mr-2" src="{{$cartItem->item->imagePath()}}" alt="{{$cartItem->sku}}" height="80" width="80">
                                    <div class="d-inline-flex flex-column">
                                            <h4 class="m-0">{{$cartItem->sku}}</h4>
                                            <p class="m-0"><small>{{$cartItem->item->iidesc}}</small></p>
                                    </div>
                                </div>
                            </td>
                            <td class="align-middle">
                                <form action="/cartItem/{{$cartItem->id}}" method="POST">
                                    @csrf
                                    @method('PATCH')
                                    <div class="d-flex flex-column align-items-center">
                                        <div class="col-sm-6" style="flex: 1 1 auto;">
                                            <input class="form-control form-control-lg text-center" type="text" name="quantity" value="{{$cartItem->quantity}}">
                                        </div>
                                        <div>
                                            <button type="submit" class="btn btn-link my-0 py-0">update</button>
                                        </div>
                                    </div>
                                </form>
                            </td>
                            <td class="text-center align-middle">
                                <a href="#" class="text-danger" title="Remove from Cart" data-toggle="modal" 
                                    data-target="#deleteFromCartModal" data-id="{{$cartItem->id}}" data-sku="{{$cartItem->sku}}" data-quantity="{{$cartItem->quantity}}" 
                                    data-description="{{$cartItem->item->iidesc}}" data-action="/cartitems/{{$cartItem->id}}">
                                        <i class="material-icons">delete</i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>  
        <div class="d-flex justify-content-center">
            <table class="table table-sm table-borderless max-width-md text-right">
                <thead>
                    <tr>
                        <th></th>
                        <th width="30%"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>Total Items:</th>
                        <td>{{$cartItems->sum('quantity')}}</td>
                    </tr>
                    <tr>
                        <th>Total Retail Price:</th>
                        <td>{{$totalRetailPrice}}</td>
                    </tr>
                    <tr>
                        <th>Store Number:</th>
                        <td><span class="store-number">Unselected</span></td>
                    </tr>
                    <tr>
                        <th>Store Address:</th>
                        <td>
                            <div class="d-flex flex-column">
                                <span class="store-address"></span>
                                <span class="store-city-state-zip"></span>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    @else
        <div class="d-flex justify-content-center">
            <div class="jumbotron">
                <div class="container">
                    <h1 class="display-4">No Items In The Cart!</h1>
                    <p class="lead">Visit the <a href="/">catalog</a> to add items to the cart. You can then come back here to order the items selected.</p>
                </div>
            </div>
        </div>
    @endif

    @if(count($cartItems) > 0)
        <div class="d-flex justify-content-end">
            <a href="#" class="btn btn-primary pl-3" data-toggle="modal" data-target="#confirmOrderModal"> 
                Continue With Order <i class="material-icons">arrow_right</i>
            </a>
        </div>
    @endif

    @include('partials.cart.deleteModal')
    @include('partials.cart.orderModal')

@endsection

@section('js')
    <script src="{{ asset('js/partials/cart/deleteModal.js') }}"></script>
    <script>
        $(function() {
            $('#order-store').val(localStorage.getItem("store"));
        });
    </script>
@endsection