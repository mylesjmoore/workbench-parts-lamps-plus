@extends('layouts.app')

@section('content')

    <div class="mt-5 mb-3">
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <h1>Orders</h1>
            </div>
            <div>
                <a href="/" class="btn btn-secondary pr-3"><i class="material-icons">arrow_left</i> Return To Catalog</a>
            </div>
        </div>
    </div>

    <div class="d-flex justify-content-center">
        @if(count($orders) > 0)
        <table class="table" style="max-width: 970px;">
            <thead>
                <tr>
                    <th>View Items</th>
                    <th>Items Ordered</th>
                    <th>Ordered Quantity</th>
                    <th>Order Status</th>
                    <th>Ordered</th>
                </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td class="align-middle">
                            <a class="btn btn-primary" href="/order/{{$order->id}}">
                                <i class="material-icons">view_list</i> Items
                            </a>
                        </td>
                        <td class="align-middle">{{$order->items->count('quantity')}}</td>
                        <td class="align-middle">{{$order->items->sum('quantity')}}</td>
                        <td class="align-middle">{{$order->status()}}</td>
                        <td class="align-middle">{{$order->created_at->toFormattedDateString()}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @else
            <div class="jumbotron ">
                <div class="container">
                    <h1 class="display-4">No orders found!</h1>
                    <p class="lead">Visit the <a href="/">catalog</a> to add items to the cart. You can then visit the cart to order the selected items.</p>
                </div>
            </div>
        @endif
    </div>

    {{$orders->links()}}

@endsection
