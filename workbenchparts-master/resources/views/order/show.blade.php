@extends('layouts.app')

@section('content')

    <div class="mt-5 mb-3">
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <h1>Order: <span class="text-muted">{{$order->created_at->toFormattedDateString()}}</span></h1>
            </div>
            <div>
                <a href="/" class="btn btn-secondary pr-3"><i class="material-icons">arrow_left</i> Return To Catalog</a>
            </div>
        </div>
    </div>
    
    @if(session()->has('message'))
        <div class="d-flex justify-content-center">
            <div class="alert alert-success alert-dismissible border border-success text-center min-width-md" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ session()->get('message') }}
            </div>
        </div>
    @endif

    <div class="d-flex justify-content-center">
        <table class="table table-borderless max-width-md">
            <thead class="border-bottom">
                <tr>
                    <th scope="col" class="text-left">Item</th>
                    <th scope="col" class="text-center">Quantity</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order->items as $item)
                    <tr class="border-bottom">
                        <td class="align-middle">
                            <div class="d-flex align-items-center">
                                <img class="mr-2" src="{{$item->item->imagePath()}}" alt="{{$item->sku}}" height="80" width="80">
                                <div class="d-inline-flex flex-column">
                                        <h4 class="m-0">{{$item->sku}}</h4>
                                        <p class="m-0"><small>{{$item->item->iidesc}}</small></p>
                                </div>
                            </div>
                        </td>
                        <td class="align-middle text-center">{{$item->quantity}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="d-flex justify-content-center">
        <table class="table table-sm table-borderless max-width-md text-right">
            <thead>
                <tr>
                    <th></th>
                    <th width="30%"></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>Total Items:</th>
                    <td>{{$order->items->sum('quantity')}}</td>
                </tr>
                <tr>
                    <th>Total Retail Price:</th>
                    <td>{{$order->totalRetailPrice()}}</td>
                </tr>
            </tbody>
        </table>
    </div>

@endsection
