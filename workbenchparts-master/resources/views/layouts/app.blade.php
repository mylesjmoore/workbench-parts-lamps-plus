<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Workbench Parts') }}</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">    
    <link href="{{ asset('css/lamps.css') }}" rel="stylesheet">
    <!-- need to put stores and description in js variables so they can be used in lamps.js -->
    <script>var stores = {!! $stores !!};</script>
    <script>var description = {!! $description  !!}</script>

</head>
<body>
    <div id="app">

        @include('nav.top')

        <main role="main" class="container-fluid py-3 px-5">
            @yield('content')
        </main>

        @include('partials.storeSelectionModal')

    </div>

    <script src="{{ asset('js/jquery-3.3.1.slim.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/lamps.js') }}"></script>
    @yield('js')
</body>
</html>