@extends('layouts.app')

@section('content')

    @include('partials.flashMessages')

    <div class="mt-5 mb-3">
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <h1>Requested Items</h1>
            </div>
            <div>
                <a href="/" class="btn btn-secondary pr-3"><i class="material-icons">arrow_left</i> Return To Catalog</a>
            </div>
        </div>
    </div>

    <div class="d-flex justify-content-center">
        @if(count($items) > 0)
            <table class="table" style="max-width: 970px;">
                <thead>
                    <tr>
                        <th>Item Requested</th>
                        <th>Requested Quantity</th>
                        <th>Requested Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($items as $item)
                        <tr>
                            <td class="align-middle">{{$item->sku}}</td>
                            <td class="align-middle">{{$item->quantity}}</td>
                            <td class="align-middle">{{$item->created_at->toFormattedDateString()}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @else
            <div class="jumbotron">
                <div class="container">
                    <h1 class="display-4 px-5">No Items Found!</h1>
                    <p class="lead">Visit the <a href="/">catalog</a> to request items.</p>
                </div>
            </div>
        @endif
    </div>

    {{$items->links()}}

@endsection
