<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\CartItem;
use Faker\Generator as Faker;

$factory->define(CartItem::class, function (Faker $faker) {
    return [
        'store' => '1',
        'sku' => 'E0005',
        'quantity' => $faker->numberBetween(1,100)
    ];
});