<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'sku' => 'E0005',
        'quantity' => '1',
        'sequence' => '1'
    ];
});