<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'store' => '1'
    ];
});

$factory->afterCreating(Order::class, function ($order, $faker) {
    $order->items()->save(factory(App\OrderItem::class)->make());
});