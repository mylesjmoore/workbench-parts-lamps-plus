<?php

Route::redirect('/', '/workbench');

Route::get('/workbench', 'PageController@workbench');
Route::get('/expense', 'PageController@expense');
Route::get('/display', 'PageController@display');

Route::get('/cart/store/{store}', 'CartItemController@index');
Route::post('/cartItem', 'CartItemController@store');
Route::patch('/cartItem/{cartItem}', 'CartItemController@update');
Route::delete('/cartItem/{cartItem}', 'CartItemController@destroy');

Route::get('/requestItem/store/{store}', 'RequestItemController@index');
Route::post('/requestItem', 'RequestItemController@store');

Route::get('/order/store/{store}', 'OrderController@index');
Route::get('/order/{order}', 'OrderController@show');
Route::post('/order', 'OrderController@store');
