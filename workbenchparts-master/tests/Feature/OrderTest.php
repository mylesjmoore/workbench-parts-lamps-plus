<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Order;
use App\CartItem;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTest extends TestCase
{
    /** @test */
    public function order_list_can_be_displayed()
    {
        //create 2 orders
        $orders = factory(Order::class, 2)->create();

        //when visiting orders for the store, expect to see both orders order date
        $this->get("/order/store/{$orders[0]->store}")
            ->assertSee($orders[0]->created_at->toFormattedDateString())
            ->assertSee($orders[1]->created_at->toFormattedDateString());

        //delete order items and orerds
        $orders->each(function($order, $key) {
            $order->items()->delete();
            $order->delete();
        });
    }

    /** @test */
    public function order_can_be_displayed()
    {
        //create an order
        $order = factory(Order::class)->create();
        
        //when visiting the order, expect to see the order item's sku
        $this->get("/order/{$order->id}")->assertSee($order->items()->first()->sku);

        //delete order items and order
        $order->items()->delete();
        $order->delete();
    }

    /** @test */
    public function orders_can_be_created()
    {
        $this->withoutExceptionHandling();

        //put an item in the cart
        $item = factory(CartItem::class)->create();

        $attributes = [
            'store' => $item->store
        ];

        //send request for a new order at the cart items store
        $response = $this->post('/order', $attributes);

        //get the order just created
        $order = Order::latest()->first();

        //expect redirect to the order just created
        $response->assertRedirect("/order/{$order->id}");

        //expect the new order's store number is the same as the item's store 
        $this->assertEquals($order->store, $attributes['store']);

        //expect that the order's items 
        $this->assertEquals(
            $order->items()->select('order_id', 'sku', 'quantity')->first()->toArray(), [
                'order_id' => $order->id,
                'sku' => $item->sku,
                'quantity' => $item->quantity
            ]
        );

        //expect that the items are no longer in the cart
        $this->assertDatabaseMissing('cart_items', $item->toArray());

        //delete the order items and the order
        $order->items()->delete();        
        $order->delete();
    }
}