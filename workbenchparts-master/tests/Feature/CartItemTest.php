<?php

namespace Tests\Feature;

use App\Item;
use App\ItemExtension;
use App\CartItem;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CartItemTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function cart_item_can_be_displayed()
    {        
        //create cart item
        $item = factory(CartItem::class)->create();

        //when visiting cart, expect to see cart item sku
        $this->get("/cart/store/{$item->store}")->assertSee($item->sku);
        
        //delete cart item
        $item->delete();
    }

    /** @test */
    public function item_can_be_added_to_cart()
    {
        //find an item with a max orderable quantity
        $sku = DB::connection('ibmi')->table('iwitemtl')
                ->where('mwexpmaxoq', '>', '0')->first()->mwskuno;
                
        //get the item
        $item = Item::where('iikwrd', $sku)->first();

        //create request attributres
        $attributes = [
            'store' => '1',
            'sku' => $item->iikwrd,
            'quantity' => ItemExtension::maxOrderableQuantity($sku)
        ];
        
        //make request to add item to cart
        $this->post('/cartItem', $attributes);

        //expect database to have the cart item
        $this->assertDatabaseHas('cart_items', $attributes);

        //delete the cart item
        CartItem::where('sku', $item->iikwrd)->delete();
    }

    /** @test */
    public function item_in_cart_cant_be_added_to_cart()
    {
        //find item with quantity
        $sku = DB::connection('ibmi')->table('iwitemtl')
                ->where('mwexpmaxoq', '>', '0')->first()->mwskuno;

        //add item to cart
        $item = factory(CartItem::class)->create([
            'sku' => $sku,
            'quantity' => 1
        ]);

        //get original number of items in cart
        $originalCount = CartItem::where('store', $item->store)->where('sku', $item->sku)->count();

        //make request to add same item to the cart
        $this->post('/cartItem', [
            'store' => $item->store,
            'sku' => $item->sku,
            'quantity' => '1'
        ]);

        //expect item in cart has not changed 
        $this->assertDatabaseHas('cart_items', [
            'store' => $item->store,
            'sku' => $item->sku,
            'quantity' => $item->quantity
        ]);        
        
        //get the new count of items in the cart
        $newCount = CartItem::where('store', $item->store)->where('sku', $item->sku)->count();
        
        //expect number of items in cart has not changed
        $this->assertEquals($originalCount, $newCount);

        //delete the cart item
        $item->delete();
    }

    /** @test */
    public function cart_item_quantity_can_be_changed()
    {
        //sku with max orderable quantity over 1
        $sku = DB::connection('ibmi')->table('iwitemtl')
                ->where('mwexpmaxoq', '>', '1')->first()->mwskuno;

        $maxOrderableQuantity = ItemExtension::maxOrderableQuantity($sku);

        //create cart item with max orderable quantity
        $item = factory(CartItem::class)->create([
            'sku' => $sku,
            'quantity' => $maxOrderableQuantity
        ]);

        //request quantity change to 1
        $this->patch("/cartItem/{$item->id}", [
            'quantity' => 1
        ]);

        //database should have quantity 1 for the item
        $this->assertDatabaseHas('cart_items', [
            'store' => $item->store,
            'sku' => $item->sku,
            'quantity' => 1 
        ]);

        $item->delete();
    }

    /** @test */
    public function items_can_be_removed_from_cart()
    {
        //add item to cart
        $item = factory(CartItem::class)->create();

        //make request to delete cart item
        $this->delete("/cartItem/{$item->id}");

        //expect database to not have cart item
        $this->assertDatabaseMissing('cart_items', $item->toArray());
    }

    /** @test */
    public function cant_add_to_cart_with_quantity_higher_then_allowed()
    {
        //sku with a limit on max orderable quantity
        $sku = DB::connection('ibmi')->table('iwitemtl')
                ->where('mwexpmaxoq', '>', '0')->first()->mwskuno;

        //set attribute request quantity max orderable quantity + 5
        $attributes = [
            'store' => '1',
            'sku' => $sku,
            'quantity' => ItemExtension::maxOrderableQuantity($sku) + 5
        ];

        //make request to add item to cart
        $this->post('/cartItem', $attributes);

        //expect that the requested item is not in the cart
        $this->assertDatabaseMissing('cart_items', $attributes);
    }

    /** @test */
    public function cant_update_quantity_higher_then_allowed()
    {
        //get sku with max orderable quantity
        $sku = DB::connection('ibmi')->table('iwitemtl')
                ->where('mwexpmaxoq', '>', '0')->first()->mwskuno;

        $maxOrderableQuantity = ItemExtension::maxOrderableQuantity($sku);

        //add item to with the max orderable quantity
        $item = factory(CartItem::class)->create([
            'sku' => $sku,
            'quantity' => $maxOrderableQuantity
        ]);
        
        //make request to update cart item quantity higher than max orderable quantity
        $this->patch("/cartItem/{$item->id}", [
            'quantity' => $maxOrderableQuantity + 5
        ]);

        //expect that the original quantity in the cart has not changed
        $this->assertDatabaseHas('cart_items', [
            'store' => $item->store,
            'sku' => $item->sku,
            'quantity' => $item->quantity
        ]);

        //delete cart item
        $item->delete();
    }
}