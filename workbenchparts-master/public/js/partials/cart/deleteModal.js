$(function () {
    $('#deleteFromCartModal').on('show.bs.modal', function (event) {

        let data = $(event.relatedTarget).data();

        $('#skuModalInput').val(data.sku);
        $('#storeModalInput').val(localStorage.getItem("store"));
        $("#modalTitle").html(data.sku);
        $("#modalDescription").html("<small>" + data.description + "</small>");
        $("#modalImg").attr("src", "http://image.lampsplus.com/is/image/" + data.sku + ".fpx?qlt=100&amp;wid=300&amp;hei=300&amp;fmt=jpeg");
        $('#quantity').val(data.quantity);

        $('#delete-cart-action').attr('action', '/cartItem/' + data.id);

    });
});