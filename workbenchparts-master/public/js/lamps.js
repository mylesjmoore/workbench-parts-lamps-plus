//any form submit will prevent submit if it has already been submitted
$('form').submit(function (event) {
    if ($(this).hasClass('submitted')) {
        event.preventDefault();
    } else {
        $(this).find(':submit').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');
        $(this).addClass('submitted');
    }
});

//verify store number is avilable in local storage on all page loads
$(function () {
    var store = localStorage.getItem("store");

    //if no store is selected, display store selection modal
    if (!store) {
        $('#storeSelectionModal').modal('toggle');
        return;
    }

    //display the store information
    updateDisplayedStore(store);

    //set cart link to the correct store
    $('#cart-link').attr('href', '/cart/store/' + store);
    $('#orders-link').attr('href', '/order/store/' + store);
    $('#request-items-link').attr('href', '/requestItem/store/' + store);

    //need to append the URL query search for item description to the pagination links
    $('a.page-link').each(function () {
        var $this = $(this);
        var _href = $this.attr("href");
        $this.attr("href", _href + '&description=' + description);
    });
});

$(function () {
    $(".hoverable").hover(
        function () {
            $(this).toggleClass("shadow");
        }
    );
});

//re-display store selection modal if no store was selected
$('#storeSelectionModal').on('hidden.bs.modal', function (event) {
    if (!localStorage.getItem("store")) {
        $('#storeSelectionModal').modal('toggle');
    }
});

//update store selection modal input with currently stored store number
$('#storeSelectionModal').on('show.bs.modal', function (event) {
    $('#store-select-input').val(localStorage.getItem("store"));
});

//when store is selected, put store into local storage and redirect home
$('#store-select-action').submit(function (event) {
    event.preventDefault();

    store = $('#store-select-input').val();

    if (store) {
        localStorage.setItem("store", store);
        location.replace("/");
    }

});

function updateDisplayedStore(store) {
    var address = '';
    var cityStateZip = '';

    $.each(stores, function (key, value) {
        if (store == value.lploc) {
            address = value.mpadr1;
            cityStateZip = value.mpcity.trim() + ', ' + value.mpstte.trim() + ' ' + value.mpzpcd;
        }
    });

    $('.store-number').html(store);
    $('.store-address').html(address);
    $('.store-city-state-zip').html(cityStateZip);
}